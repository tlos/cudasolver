#ifndef __PARAM__STATIC__VERIFIER__H
#define __PARAM__STATIC__VERIFIER__H

#include "Configuration.h"
#include "GpuConfiguration.h"


#define CHECK_IF_POWER_OF_TWO(X) (((X) > 0) && ((X) & ((X)-1)) == 0  )


// sprawdzenie czy liczba element�w sko�czonych jest pot�g� 2ki
#if CHECK_IF_POWER_OF_TWO(N) == 0 
	#error macro_N is not power of two !!
#endif


// sprawdzenie stopnia kwadratury Gaussa
#if GAUSS_QUADRATURE_ORDER != 3 && GAUSS_QUADRATURE_ORDER != 5 && GAUSS_QUADRATURE_ORDER != 11
	#error macro_GAUSS_QUADRATURE_ORDER has wrong value !!!
#endif



// INTEGRATION_INTERVALS czy pot�ga 2ch ? 
#if CHECK_IF_POWER_OF_TWO(INTEGRATION_INTERVALS) == 0
	#error macro_INTEGRATION_INTERVALS is not power of two !!
#endif

// czy P_ORDER >= 1
#if P_ORDER < 1
	#error macro_P_ORDER is smaller than 1
#endif



//GPUConfig
#if CHECK_IF_POWER_OF_TWO(THREADS_PER_BLOCK) == 0 || THREADS_PER_BLOCK < 2 
	#error wrong value in macro_THREADS_PER_BLOCK
#endif
#if CHECK_IF_POWER_OF_TWO(PHISICAL_BLOCKS_NUMBER) == 0 || PHISICAL_BLOCKS_NUMBER < 1 
	#error wrong value in macro_PHISICAL_BLOCKS_NUMBER
#endif
#if CHECK_IF_POWER_OF_TWO(THREADS_IN_TOP_BLOCK) == 0 || THREADS_IN_TOP_BLOCK < 2 
	#error wrong value in macro_THREADS_IN_TOP_BLOCK
#endif



#endif