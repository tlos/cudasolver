#ifndef SOLVER_CONFIGURATION_H_
#define SOLVER_CONFIGURATION_H_

#define DATA_TYPE float
#define MES

#include "Configuration.h"
#include "Utils.h"
#include "PropertyReader.h"
#include "RunConfigResolver.h"
#include "GpuConfiguration.h"

	
class SolverConfiguration{

public:
	SolverConfiguration() : 
		is_file_to_write_output(false),
		is_auto_config_run(true),
		device_id(DEFAULT_DEVICE_ID),
		print_config(false)
	{
		/*Constructor*/
	}

	void load_config(){
		internals_number = N;

		if( this->is_auto_config_run ){
			RunConfigResolver run_config_resolver(device_id);
			threads_per_block = run_config_resolver.thread_number_in_bottom_block;
			phisical_blocks_number = run_config_resolver.blocks_number_in_bottom_block;
			threads_in_top_block = run_config_resolver.threads_number_in_top_block;
		}else{
			threads_per_block = THREADS_PER_BLOCK;
			phisical_blocks_number = PHISICAL_BLOCKS_NUMBER;
			threads_in_top_block = THREADS_IN_TOP_BLOCK;
		}
		init();
	}
	void print(){
		printf("###SolverConfiguration###\n");
		printf("   DEVICE SETTINGS:\n");
		printf("threads_per_block=\t%d\n", threads_per_block);
		printf("phis_blocks_number=\t%d\n", phisical_blocks_number);
		printf("logi_blocks_number=\t%d\n", logic_blocks_number);
		printf("threads_in_top_block=\t%d\n", threads_in_top_block);
		printf("integration_intervals=\t%d\n", INTEGRATION_INTERVALS);
	
		printf("   SOLVER DEBUG INFO:\n");
		printf("nodes_in_bottom_block=\t%d\n", nodes_in_bottom_block);
		printf("nodes_in_top_block=\t%d\n", nodes_in_top_block);
		printf("levels_in_bottom_block=\t%d\n", levels_in_bottom_block);
		printf("levels_in_top_block=\t%d\n", levels_in_top_block);
		printf("levels_in_grid=\t\t%d\n", levels_in_grid);
	
		printf("   SOLUTION SETTINGS:\n");
		printf("P order=\t%d\n", P_ORDER);
		printf("internals_number=\t%d\n", internals_number);
		printf("internal_len=\t\t%f\n", internal_len);
		printf("points_to_plot=\t%d\n", points_to_plot);

		printf("   EQUATION SETTINGS:\n");
		printf("P=\t\t%f\n", P);
		printf("Q=\t\t%f\n", Q);
		printf("D=\t\t%f\n", D);
		printf("BETA=\t\t%f\n", BETA);
		printf("GAMMA=\t\t%f\n", GAMMA);
		printf("###SolverConfiguration-END###\n");

	}

	string file_name_to_write_output;

	bool is_file_to_write_output;
	bool is_auto_config_run;

	unsigned int threads_per_block;
	unsigned int phisical_blocks_number;
	unsigned int internals_number;
	
	/* tyle ile powinno by� blok�w  */
	unsigned int logic_blocks_number;

	unsigned int nodes_in_bottom_block;
	unsigned int nodes_in_top_block;

	unsigned int levels_in_bottom_block;
	unsigned int levels_in_top_block;
	unsigned int levels_in_grid;

	
	unsigned int threads_in_top_block;

	unsigned int points_to_plot;

	int device_id;


	DATA_TYPE internal_len;

	int p_orders_tab[P_ORDER + 1];
	bool print_config;

private:
	void init(){
		logic_blocks_number = internals_number / threads_per_block;
	
		nodes_in_bottom_block = internals_number / logic_blocks_number;
		nodes_in_top_block = logic_blocks_number;

		levels_in_bottom_block	= log2( nodes_in_bottom_block ) + 1;
		levels_in_top_block		= log2( nodes_in_top_block ) + 1;
		levels_in_grid			= log2( internals_number ) + 1;
	
		internal_len = (Q - P)/(DATA_TYPE)internals_number; 
		points_to_plot = (POINTS_TO_PLOT);

		threads_in_top_block = phisical_blocks_number;
	
		// init p-orders-tab
		for (int i = 0; i <= P_ORDER - 2; i++){
			p_orders_tab[i] = P_ORDER + i;
		}
		p_orders_tab[P_ORDER - 1] = 1;
		p_orders_tab[P_ORDER] = 2;
		// end init

	}

};



#endif