#ifndef PROPERTY_READER_H_
#define PROPERTY_READER_H_

#include <cstdio>
#include <cstdlib>
#include <string>
#include <map>
#include <fstream>
#include <iostream>
#include <sstream>

using namespace std;

class PropertyReader{
public:
	PropertyReader():delimeter("=")
	{

	}
	bool load_properties(const std::string file_name){
		std::ifstream infile(file_name.c_str());
		if(!infile.is_open()){
			std::cerr << "Cannot open file!" << std::endl;
			return false;
		}
		string line;
		while(std::getline(infile, line)){
			unsigned int idx = line.find(delimeter);
			if( idx == string::npos ){
				infile.close();
				std::cerr << "Wrong file format!" << std::endl;
				return false;
			}
			string startLetter = line.substr(0,1);
			if( startLetter == "#" ) continue;
			string key = line.substr(0,idx);
			string value = line.substr(idx + delimeter.length());
			properties[key] = value;
		}
		infile.close();
		return true;
	}
	std::string & operator[](const std::string key){
		return properties[key];
	}
	std::string & get(const std::string key){
		return this->operator[](key);
	}
	unsigned int get_int(const std::string key){
		string value = get(key);
		int result;
		istringstream iss(value);
		iss >> result;
		return result;
	}
	float get_float(const std::string key){
		string value = get(key);
		float result;
		istringstream iss(value);
		iss >> result;
		return result;
	}



private:
	std::map<std::string,std::string> properties;
	std::string delimeter;
};


#endif