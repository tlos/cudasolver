﻿#ifndef CONFIGURATION_CUDA_SOLVER_H
#define CONFIGURATION_CUDA_SOLVER_H


/**
* Parametry równania
*/

// p - początek przedziału
// q - koniec przedziału
#define P (1.00f)
#define Q (2.00f)

// wartość warunku Dirichleta w punkcie P
#define D (0.00f) 

#define BETA (1.00f)
#define GAMMA (2.00f)

// liczba elementów skończonych
#define N (16)

// ilość punktów dla wykresu i wyniku, domyślnie (2*N +1)
#define POINTS_TO_PLOT ( 2*N + 1 )

//stopień funkcji CHI - KSZTAŁTU
#define P_ORDER (3)


//dokładnośc całkowania, powinna być to wielokrotność 2
#define INTEGRATION_INTERVALS (32)


//stopień kwadratury Gaussa 
//możliwe wartości: 3,5,11
#define GAUSS_QUADRATURE_ORDER (5)

#endif