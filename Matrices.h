#ifndef MATRICES_H_
#define MATRICES_H_

#include "cuda_runtime_api.h"
#include "SolverConfiguration.h"

// mapping vector to PxP frontal matrix
// for p=3:
// P: 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, ...
// A:             B:
// 0  1  2  3    16
// 4  5  6  7    17
// 8  9 10 11    18
//12 13 14 15    19

// x - row; y - column
__device__ __host__
DATA_TYPE get_A_idx_from_P_frontal(int x, int y, int internalId){
	return (DATA_TYPE) (P_ORDER + 1) * (P_ORDER + 2) * internalId + (P_ORDER + 1) * x + y;
}

__device__ __host__
DATA_TYPE get_B_idx_from_P_frontal(int x, int internalId){
	return (DATA_TYPE) (P_ORDER + 1) * (P_ORDER + 2) * internalId + (P_ORDER + 1) * (P_ORDER + 1) +  x;
}

// FrontalMatrix2 * FrontalMatrix2 = FrontalMatrix3
#define MERGE_MATRICES(M1,M2,RESULT) \
do{\
	RESULT.A[0][0] = M1.A[0][0];\
    RESULT.A[0][1] = M1.A[0][1];\
    RESULT.A[0][2] = 0.0;\
    RESULT.A[1][0] = M1.A[1][0];\
    RESULT.A[1][1] = M1.A[1][1] + M2.A[0][0];\
    RESULT.A[1][2] = M2.A[0][1];\
    RESULT.A[2][0] = 0.0;\
    RESULT.A[2][1] = M2.A[1][0];\
    RESULT.A[2][2] = M2.A[1][1];\
	;\
    RESULT.B[0] = M1.B[0];\
    RESULT.B[1] = M1.B[1] + M2.B[0];\
    RESULT.B[2] = M2.B[1]; \
    ;\
	RESULT.X_id[0] = M1.X_id[0];\
    RESULT.X_id[1] = M1.X_id[1]; /* albo = M2X-id[0] -> tutaj nie ma znaczenia  */ \
    RESULT.X_id[2] = M2.X_id[1];\
}while(0)

struct FrontalMatrix2{
	// A * X = B
    DATA_TYPE A[2][2];	// macierz A - wsp�czynniki
    DATA_TYPE B[2];		// macierz B - wyniki
    int X_id[2];	// u1, u2 ;; macierz X - indeksy niewiadomych
};

struct FrontalMatrix3{
	// analogicznie jak wy�ej
    DATA_TYPE A[3][3];
    DATA_TYPE B[3];
    int X_id[3];	// u1, u2, u3
};

#endif
