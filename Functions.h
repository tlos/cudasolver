﻿#ifndef __FUNCTIONS_TT_H
#define __FUNCTIONS_TT_H

#define MES_FUNCTION __device__ __forceinline__ DATA_TYPE

MES_FUNCTION fun_a( DATA_TYPE x ){
	return 1.0;
}

MES_FUNCTION fun_b( DATA_TYPE x ){
	return 0.0;
}
MES_FUNCTION fun_c( DATA_TYPE x ){
	return 0.0;
}

MES_FUNCTION fun_f( DATA_TYPE x ){
	return 1;//4.0 + 6.0 * x + 2.0*(x*x);
}

#endif