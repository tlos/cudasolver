
#ifndef PARTIAL_GAUSS_ELIMINATION_H__
#define PARTIAL_GAUSS_ELIMINATION_H__

#include "DataProvider.h"
#include "Configuration.h"
#include "Utils.h"
#include "cuda_runtime.h"
#include "device_launch_parameters.h"
#include "ComputingPhrase.h"
#include "Stack.h"

__device__ void partial_gauss_part_two(SolverConfiguration &config, DATA_TYPE *dev_solver_solution_vector, DATA_TYPE *dev_frontal_matrices_p_x_p, DATA_TYPE *dev_complete_solution_vector)
{
	unsigned int logic_tid = GET_LOGIC_TID;
	unsigned int internal_id_inc = GET_LOGIC_TID_INC;

	int index;
	// skopiowanie rozwiazania z solwera do macierzy frontalnych 4x4
	for( int internal_id = logic_tid; internal_id < N; internal_id += internal_id_inc ) {
		unsigned int p = P_ORDER;
		
		index = get_A_idx_from_P_frontal(p-1, p-1, internal_id);
		dev_frontal_matrices_p_x_p[index] = 1.0;

		index = get_A_idx_from_P_frontal(p-1, p, internal_id);
		dev_frontal_matrices_p_x_p[index] = 0.0;

		index = get_A_idx_from_P_frontal(p, p-1, internal_id);
		dev_frontal_matrices_p_x_p[index] = 0.0;

		index = get_A_idx_from_P_frontal(p, p, internal_id);
		dev_frontal_matrices_p_x_p[index] = 1.0;

		unsigned int offset = (p + 1) * internal_id; // przesuniecie w wektorze z ca�ym rozwiazaniem
		dev_complete_solution_vector[offset + p -1 ] = dev_solver_solution_vector[internal_id];
		dev_complete_solution_vector[offset + p ] = dev_solver_solution_vector[internal_id + 1];		
	}

	__syncthreads();
	// czesciowa elminacja gaussa -> wyliczenie rozwiazania pozostalych wspolczynnikow i zapisanie do wektora z calym rozwiazaniem
	for( int internal_id = logic_tid; internal_id < N; internal_id += internal_id_inc ) {		
		unsigned int p = P_ORDER;
		unsigned int offset = (p + 1) * internal_id; // przesuniecie w wektorze z ca�ym rozwiazaniem

		for ( int k = p - 2; k >= 0; k--){
			DATA_TYPE sum = 0.0;
			for (int i = k + 1; i < p+ 1; i++){
				index = get_A_idx_from_P_frontal(k, i, internal_id);
				sum += dev_frontal_matrices_p_x_p[index] * dev_complete_solution_vector[offset + i];
			}
			int index_B = get_B_idx_from_P_frontal(k, internal_id);
			int index_A = get_A_idx_from_P_frontal(k, k, internal_id);

			dev_complete_solution_vector[offset + k] = ( dev_frontal_matrices_p_x_p[index_B] - sum ) / dev_frontal_matrices_p_x_p[index_A];			
		}		
	}

}
#endif