#ifndef TOMTOM_UTILS_H_
#define TOMTOM_UTILS_H_

#include <cuda_runtime_api.h>
#include <cstdio>
#include <cstdlib>


#define IF_NOT_NULL( POINTER, CMD ) if( (POINTER) != NULL) CMD 
#define GET_LOGIC_TID		(threadIdx.x +blockIdx.x*blockDim.x)
#define GET_LOGIC_TID_INC	(blockDim.x* gridDim.x)

static void HandleError( cudaError_t err,
                         const char *file,
                         int line ) {
    if (err != cudaSuccess) {
        printf( "%s in %s at line %d\n", cudaGetErrorString( err ),
                file, line );
        exit( EXIT_FAILURE );
    }
}
#define HANDLE_ERROR( err ) (HandleError( err, __FILE__, __LINE__ ))


#define HANDLE_NULL( a ) {if (a == NULL) { \
                            printf( "Host memory failed in %s at line %d\n", \
                                    __FILE__, __LINE__ ); \
                            exit( EXIT_FAILURE );}}


/**
* Funkcja podnosi dwójkę do n-tej potęgi
* 2^n
* @param 	n - współczynnik potęgi dwójki
* @return	2^(n) 	
*/
__device__ __host__
inline unsigned int two_powered_times(unsigned int n){
    unsigned int result = 1;
    for( unsigned int i=0; i < n; i++){
        result*=2;
    }
    return result;
}

/**
* Funkcja zamienia a <-> b
*/
template<typename T>
__device__ __host__
inline void swap_us(T& a, T& b){
	T tmp = a;
	a = b;
	b = tmp;
};


/**
* Funkcja obliczająca logarytm o podstawie 2 z b.
* @param 	b -	liczba, z której ma powstać logarytm
*/
__device__ __host__
inline unsigned int log2( unsigned int b )
{
	unsigned int ans = 0 ;
	while( b >>= 1 ) ans++;
	return ans ;
}

#endif
