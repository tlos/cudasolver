#ifndef STACK_H_
#define STACK_H_
#include "cuda_runtime_api.h"
#include "Matrices.h"
#include <cstdio>

class Stack{
public:	
	Stack(const unsigned int internals_number, const unsigned int chunk_size){
		this->internals_number = internals_number;
		this->internals_number /= 2;
		this->chunk_size = chunk_size;
		init();
	}
	~Stack()
	{

	}
	 void clean()
	{
		cudaFree(d_stack);
		cudaFree(d_pointers);
	}

	 Stack* copy_to_device()
	 {
		 Stack *p;
		 HANDLE_ERROR( cudaMalloc( (void**)&p, sizeof(Stack) ) );
		 HANDLE_ERROR( cudaMemcpy( p, this, sizeof(Stack), cudaMemcpyHostToDevice ) );
		 return p;
	 }

	 static Stack* copy_from_device(Stack *dev_p){
		 Stack *p = (Stack*)malloc( sizeof(Stack) );
		 HANDLE_ERROR( cudaMemcpy( p, dev_p, sizeof(Stack), cudaMemcpyDeviceToHost) );
		 return p;
	 }

	 static void clean_copied_stack(Stack *host_p){
		free(host_p);
	 }


	__device__ bool push(int internal_id, FrontalMatrix3 item){ 
		internal_id /= 2;
		if( internal_id*chunk_size <= d_pointers[internal_id]  && d_pointers[internal_id] < (internal_id+1)*chunk_size ){
			d_stack[ d_pointers[ internal_id ]++ ]  = item;
			return true;
		}
		return false;
	}
	__device__ bool pop(int internal_id, FrontalMatrix3 &out){
		internal_id /= 2;

		if( internal_id*chunk_size < d_pointers[internal_id]  && d_pointers[internal_id] <= (internal_id+1)*chunk_size ){
			out = d_stack[ --d_pointers[ internal_id ] ];
			return true;
		}
		return false;
	}

	__host__ void print(){
#ifdef DEBUG
		FrontalMatrix3 *h_stack = (FrontalMatrix3*) malloc( chunk_size*internals_number*sizeof(FrontalMatrix3) ); 
		int *h_pointers = (int*)malloc( internals_number*sizeof(int) );  
		
		HANDLE_ERROR( cudaMemcpy(h_pointers, d_pointers, internals_number*sizeof(int), cudaMemcpyDeviceToHost) );
		HANDLE_ERROR( cudaMemcpy(h_stack, d_stack, chunk_size*internals_number*sizeof(FrontalMatrix3), cudaMemcpyDeviceToHost) );
		printf("\n###### STACK DEBUG #####\n\n");

		for(unsigned int internal_id=0; internal_id < internals_number; internal_id++)
		{
			printf("Stack for internal_id=%d, pointer=%d\n", internal_id, h_pointers[internal_id]);	
			int up_to = h_pointers[internal_id];
			for( int i = internal_id*chunk_size; i < up_to; i++ )
			{
				//printf("\t-> %d\n", h_stack[i]);
				FrontalMatrix3 f = h_stack[i];
				printf("[\n%5.2f  %5.2f   %5.2f |  %5.2f |  %5d\n", f.A[0][0],f.A[0][1], f.A[0][2], f.B[0], f.X_id[0]  );
				printf("%5.2f  %5.2f   %5.2f |  %5.2f |  %5d\n", f.A[1][0],f.A[1][1], f.A[1][2], f.B[1], f.X_id[1]  );
				printf("%5.2f  %5.2f   %5.2f |  %5.2f |  %5d\n", f.A[2][0],f.A[2][1], f.A[2][2], f.B[2], f.X_id[2]  );
				printf("]\n");
			}
		}

		printf("\n###### STACK DEBUG END #####\n\n");

		free(h_stack);
		free(h_pointers);
#endif
	}
private:
	unsigned int chunk_size;
	unsigned int internals_number;
	int *d_pointers; // wskazuje na wolny element
	FrontalMatrix3 *d_stack;

	Stack(){}

	void init()
	{
		int *h_pointers = (int*)malloc( internals_number*sizeof(int) );
		HANDLE_ERROR( cudaMalloc((void**)&d_pointers,  internals_number*sizeof(int)) );
		for(unsigned int i=0; i < internals_number; ++i )
		{
			h_pointers[i] = i*chunk_size;
		}
		
		HANDLE_ERROR( cudaMemcpy(d_pointers, h_pointers, internals_number*sizeof(int), cudaMemcpyHostToDevice) );
		HANDLE_ERROR( cudaMalloc((void**)&d_stack,  chunk_size*internals_number*sizeof(FrontalMatrix3)) );
		
		free(h_pointers);
	}
};

#endif