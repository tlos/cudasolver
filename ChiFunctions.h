#ifndef __CHI_FUNCTIONS
#define __CHI_FUNCTIONS

#include "SolverConfiguration.h"
#include "Configuration.h"

// polynomial_order: 1, 2, 3, ...
// internalId: 0, 1, 2, 3, ...
__device__
DATA_TYPE chi( unsigned int polynomial_order, unsigned int internalId, DATA_TYPE x, const SolverConfiguration &config ){
	DATA_TYPE xL = P + internalId * config.internal_len;
	DATA_TYPE xR = xL + config.internal_len;

	if (x < xL || x > xR)
		return 0.0;

	DATA_TYPE fragment = (x - xL) / (xR - xL);

	if (polynomial_order == 1){
		return 1.0 - fragment;
	}

	if (polynomial_order == 2){
		return fragment;
	}

	if (polynomial_order == 3){
		return (1.0 - fragment) * (fragment);
	}

	DATA_TYPE power = 1.0;
	for (int i=0; i < polynomial_order - 3; i++){
		power *= (2.0 * fragment - 1.0);
	}

	return (1.0 - fragment) * fragment * power;
}

__device__
DATA_TYPE chi_dx( unsigned int polynomial_order, unsigned int internalId, DATA_TYPE x, const SolverConfiguration &config ){
	DATA_TYPE xL = P + internalId * config.internal_len;
	DATA_TYPE xR = xL + config.internal_len;
		
	if (x < xL || x > xR)
		return 0.0;

	DATA_TYPE one_by_len = 1.0 / (xR - xL);

	if (polynomial_order == 1){
		return -1.0 * one_by_len;
	}

	if (polynomial_order == 2){
		return one_by_len;
	}

	DATA_TYPE fragment = (x - xL) / (xR - xL);
	DATA_TYPE dx_for_p_3 = -1.0 * one_by_len * fragment + (1.0 - fragment) * one_by_len;

	if (polynomial_order == 3){
		return dx_for_p_3;
	}
	
	DATA_TYPE dx_part2 = 1.0;
	for (int i = 0; i < polynomial_order - 3; i++){
		dx_part2 *= ( 2.0 * fragment - 1.0);
	}

	DATA_TYPE dx_part3 = ( 1.0 - fragment) * fragment;
		
	DATA_TYPE dx_part4_power = 1.0;
	for (int i = 0; i < polynomial_order - 4; i++){
		dx_part2 *= ( 2.0 * fragment - 1.0);
	}
	DATA_TYPE dx_part4 = (polynomial_order - 3) * dx_part4_power * 2.0 * one_by_len;

	return dx_for_p_3 * dx_part2 + dx_part3 * dx_part4;
}

/**
* Funkcja odpowiadająca U^ 
* parametry polynomial_order i internalId sa nieistotne, chodzi tylko o możliwość podstawienia dxU^ w miejsce funkcji bazowych
*/
__device__
DATA_TYPE u_peak( unsigned int polynomial_order, unsigned int internalId, DATA_TYPE x, const SolverConfiguration &config ){
	return D*(Q - x)/(Q - P);
}

/**
* Funkcja odpowiadająca dxU^ 
* parametry polynomial_order i internalId sa nieistotne, chodzi tylko o możliwość podstawienia dxU^ w miejsce pochodnych funkcji bazowych
*/
__device__
DATA_TYPE u_peak_dx( unsigned int polynomial_order, unsigned int internalId, DATA_TYPE x, const SolverConfiguration &config ){
	return -D/(Q - P);
}

#endif
