#ifndef DATA_PROVIDER_H_
#define DATA_PROVIDER_H_

#include "SolverConfiguration.h"
#include "Matrices.h"
#include "Integration.h"
#include "ChiFunctions.h"

#include "Functions.h"

__device__ __forceinline__
DATA_TYPE get_x_start_for_internela_id(int internal_id, const SolverConfiguration &config){
	return config.internal_len*(DATA_TYPE)internal_id + P;
}

__device__ DATA_TYPE get_from_global_A(int internal_id, unsigned int chi_1_order, unsigned int chi_2_order, const SolverConfiguration &config){

	DATA_TYPE q = Q;

	DATA_TYPE y1, y2, y3, y4;

	DATA_TYPE qi = get_x_start_for_internela_id(internal_id, config) + config.internal_len; 
	DATA_TYPE pi = get_x_start_for_internela_id(internal_id-1, config);

	y1 = BETA * chi(chi_1_order, internal_id, q, config) * chi (chi_2_order, internal_id, q, config); 
	y2 = integrate(fun_a, chi_dx, chi_1_order, chi_dx, chi_2_order, internal_id, pi, qi, config);
	y3 = integrate(fun_b, chi_dx, chi_1_order, chi, chi_2_order, internal_id, pi, qi, config);
	y4 = integrate(fun_c, chi, chi_1_order, chi, chi_2_order, internal_id, pi, qi, config);
	 
	return -y1 - y2 + y3 + y4;
}

__device__ DATA_TYPE get_from_global_B (int internal_id, unsigned int chi_order, const SolverConfiguration &config){
	
	DATA_TYPE q = Q;

	DATA_TYPE y1, y2, y3, y4, y5, y6;

	DATA_TYPE qi = get_x_start_for_internela_id(internal_id, config) + config.internal_len; 
	DATA_TYPE pi = get_x_start_for_internela_id(internal_id-1, config);

	y1 = integrate(fun_f, chi, chi_order, internal_id, pi, qi, config);
	y2 = GAMMA * chi(chi_order, internal_id, q, config);
	y3 = BETA * u_peak (1,0, q, config);
	y4 = integrate(fun_a, u_peak_dx, 0, chi_dx, chi_order, internal_id, pi, qi, config );
	y5 = integrate(fun_b, u_peak_dx, 0, chi, chi_order, internal_id, pi, qi, config );
	y6 = integrate(fun_c, u_peak, 0, chi, chi_order, internal_id, pi, qi, config );
	
	return y1 - y2 + y3 + y4 - y5 - y6;
}

__device__ void get_frontal_p_matrix_for_internal_id(int internal_id, DATA_TYPE *frontal_p_x_p_matrices,  const SolverConfiguration &config ){
	unsigned int p_order = P_ORDER; 

	int index;
	for (int i=0; i <= p_order; i++){				
		for (int j = 0; j <= p_order; j++){			
			index = get_A_idx_from_P_frontal(i, j, internal_id);
			frontal_p_x_p_matrices[index] = get_from_global_A(internal_id, config.p_orders_tab[i], config.p_orders_tab[j], config);
		}

		index = get_B_idx_from_P_frontal(i, internal_id);
		frontal_p_x_p_matrices[index] = get_from_global_B(internal_id, config.p_orders_tab[i], config);
	}

	// warunek dirichleta w punkcie p
	if (internal_id == 0){
		for (int i = 0; i <= P_ORDER; i++){
			index = get_A_idx_from_P_frontal(P_ORDER - 1, i, internal_id);
			frontal_p_x_p_matrices[index] = 0;
		}
		index = get_A_idx_from_P_frontal(P_ORDER - 1, P_ORDER - 1, internal_id);
		frontal_p_x_p_matrices[index] = 1.0;
		index = get_B_idx_from_P_frontal (P_ORDER - 1, internal_id);
		frontal_p_x_p_matrices[index] = 0;
	}

}

#endif
