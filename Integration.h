#ifndef TT_CALKOWANIE_H_
#define TT_CALKOWANIE_H_

#include "SolverConfiguration.h"
#include "Configuration.h"

#if GAUSS_QUADRATURE_ORDER == 3

	__device__ int n_max = 3; 
	
	__device__
	DATA_TYPE tab_A[] = {
						0.8888888888888888,
						0.5555555555555556, 
						0.5555555555555556
						};

	__device__ 
	DATA_TYPE tab_x[] = {
						0.0,
						-0.7745966692414834,
						0.7745966692414834
						};

#elif GAUSS_QUADRATURE_ORDER == 5

	__device__ int n_max = 5; 

	__device__
	DATA_TYPE tab_A[] = {
						0.5688888888888889,
						0.4786286704993665,
						0.4786286704993665,
						0.2369268850561891,
						0.2369268850561891
						};

	__device__
	DATA_TYPE tab_x[] = {
						0.0,
						-0.5384693101056831,
						0.5384693101056831,
						-0.9061798459386640,
						0.9061798459386640
						};

#elif GAUSS_QUADRATURE_ORDER == 11
	__device__ int n_max = 11;
	
	__device__
	DATA_TYPE tab_A[] = {
						0.2729250867779006,
						0.2628045445102467,
						0.2628045445102467,
						0.2331937645919905,
						0.2331937645919905,
						0.1862902109277343,
						0.1862902109277343,
						0.1255803694649046,
						0.1255803694649046,
						0.0556685671161737,
						0.0556685671161737
						};

	__device__
	DATA_TYPE tab_x[] = {
						0.0,
						-0.2695431559523450,
						0.2695431559523450,
						-0.5190961292068118,
						0.5190961292068118,
						-0.7301520055740494,
						0.7301520055740494,
						-0.8870625997680953,
						0.8870625997680953,
						-0.9782286581460570,
						0.9782286581460570
						};

#endif
	

__device__ 
DATA_TYPE
integrate(
	DATA_TYPE (*fun)(DATA_TYPE x),
	DATA_TYPE (*chi_1) (unsigned int polynomial_order, unsigned int internalId, DATA_TYPE x, const SolverConfiguration &config ),
	unsigned int chi_1_order,
	DATA_TYPE (*chi_2) (unsigned int polynomial_order, unsigned int internalId, DATA_TYPE x, const SolverConfiguration &config ),
	unsigned int chi_2_order,
	int internal_id,
	DATA_TYPE p,
	DATA_TYPE q,
	const SolverConfiguration &config)
{
	int internals_num = INTEGRATION_INTERVALS;

	DATA_TYPE result = 0.0;
	DATA_TYPE h = (q - p) / ((DATA_TYPE) internals_num);

	DATA_TYPE a  = p;
	DATA_TYPE b = a + h;

	DATA_TYPE tmp_sum;
	DATA_TYPE tk;
	DATA_TYPE fun_val;
	int i;
	for (i=0; i < internals_num; i++){
		tmp_sum = 0.0;
		int k;
		for (k=0; k<n_max; k++){
			tk = (a + b)/2.0 + (b - a) * tab_x[k] / 2.0;
			fun_val = chi_1( chi_1_order, internal_id, tk, config ) * chi_2( chi_2_order, internal_id, tk, config ) * fun( tk );
			tmp_sum += tab_A[k] * fun_val;
		}

		tmp_sum *= (b - a) / 2.0;
		result += tmp_sum;
		a = b;
		b += h;
	}
	return result;
}

__device__
DATA_TYPE 
integrate(
	DATA_TYPE (*fun)(DATA_TYPE x),
	DATA_TYPE (*chi) (unsigned int polynomial_order, unsigned int internalId, DATA_TYPE x, const SolverConfiguration &config ),
	unsigned int chi_order,
	int internal_id,
	DATA_TYPE p,
	DATA_TYPE q,
	const SolverConfiguration &config)
{
	int internals_num =INTEGRATION_INTERVALS;

	DATA_TYPE result = 0.0;
	DATA_TYPE h = (q - p) / ((DATA_TYPE) internals_num);

	DATA_TYPE a  = p;
	DATA_TYPE b = a + h;

	DATA_TYPE tmp_sum;
	DATA_TYPE tk;
	DATA_TYPE fun_val;
	int i;
	for (i=0; i < internals_num; i++){
		tmp_sum = 0.0;
		int k;
		for (k=0; k<n_max; k++){
			tk = (a + b)/2.0 + (b - a) * tab_x[k] / 2.0;
			fun_val = chi( chi_order, internal_id, tk, config ) * fun( tk );
			tmp_sum += tab_A[k] * fun_val;
		}

		tmp_sum *= (b - a) / 2.0;
		result += tmp_sum;
		a = b;
		b += h;
	}
	return result;

}


#endif 
