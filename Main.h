#ifndef MAIN_H_
#define MAIN_H_

#include "cuda_runtime.h"
#include "device_launch_parameters.h"

#include "ParamStaticVerifier.h"
#include "Configuration.h"
#include <stdio.h>
#include "SolverConfiguration.h"
#include "Stack.h"
#include "DataProvider.h"
#include "PrintTools.h"
#include "Integration.h"
#include "RunConfigResolver.h"
#include "CudaDevice.h"
#include "ComputingPhrase.h"
#include "TimeMeassure.h"
#include "ArgParser.h"
#include "MultiFrontalSolver.h"
#include "DataInitKernel.h"



void initMemory();
void startSolving();
void calculateDataToPlotForCompleteSolution();
void printSolverSolution();
void printCompleteDataToPlot();
void writeCompleteSolutionToFile();
void cleanMemory();

__global__ void kernel_calculate_values_to_plot_for_complete_solution(SolverConfiguration config, DATA_TYPE *dev_complete_solution_vector, DATA_TYPE *dev_plot_complete_values);

#endif
