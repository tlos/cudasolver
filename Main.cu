#include "Main.h"

#define SYNC_THREADS __syncthreads

SolverConfiguration config;
FrontalMatrix2 *dev_frontal_matrices_2x2;
DATA_TYPE *dev_frontal_matrices_p_x_p;
DATA_TYPE *dev_solver_solution_vector;
DATA_TYPE *host_solver_solution_vector;
DATA_TYPE *dev_complete_solution_vector;
DATA_TYPE *host_complete_solution_vector;
DATA_TYPE *dev_plot_solver_values;
DATA_TYPE *host_plot_solver_values;
DATA_TYPE *dev_plot_complete_values;
DATA_TYPE *host_plot_complete_values;
Stack *dev_stack;


int main(int argc, char *argv[])
{		
	int result = parse_args_and_update_config(argc,argv, config);
	if( result == -1 || result == 0 ){
		return result;
	}

	config.load_config();
	
	if(!set_device(config.device_id)){
		printf("[ERROR] failed setting device\n");
		return -1;
	}

	if( config.print_config) 
		config.print();
	
	initMemory();
	float time = measure_runtime(startSolving);

	calculateDataToPlotForCompleteSolution();

	if( config.is_file_to_write_output) {
		writeCompleteSolutionToFile();		
	}else{
		printCompleteDataToPlot();
	}

	cleanMemory();
		
	printf("Elapsed: %f ms\n",time);
	return 0;
}

void initMemory(){


	// pamiec dla macierzy frontalnych PxP
	HANDLE_ERROR (cudaMalloc((void**)&dev_frontal_matrices_p_x_p, sizeof(DATA_TYPE) * N * (P_ORDER + 1) * (P_ORDER + 2 ) ));
	
	// pami�c dla macierzy frontalnych 2d
	HANDLE_ERROR( cudaMalloc((void**)&dev_frontal_matrices_2x2,sizeof(FrontalMatrix2)*N ) );

	// pami�c dla wynik�w solwera na gpu i ho�cie
	HANDLE_ERROR( cudaMalloc((void**)&dev_solver_solution_vector,sizeof(DATA_TYPE)*(N + 1) ) );
	host_solver_solution_vector = (DATA_TYPE*)malloc(sizeof(DATA_TYPE)*(N + 1)  );
	
	// inicjacja stosu
	{
		Stack host_stack( N , config.levels_in_grid );
		dev_stack = host_stack.copy_to_device();
	}

	// pamiec dla calych wynikow, uzyskanych po rozwiazanie macierzy 4x4
	HANDLE_ERROR( cudaMalloc((void**)&dev_complete_solution_vector, sizeof(DATA_TYPE)*(N) * (P_ORDER + 1) ) );
	host_complete_solution_vector = (DATA_TYPE*) malloc( sizeof(DATA_TYPE)*(N) * (P_ORDER + 1) );
}

void startSolving(){	
	unsigned int threads_in_top_block = config.threads_in_top_block;
	unsigned int threads_per_block = config.threads_per_block;
	unsigned int block_number = config.phisical_blocks_number;
#ifdef DEBUG
	printf("##### Starting kernel with :\n");
	printf("threads_per_block = %d\n",threads_per_block);
	printf("block_number = %d\n",block_number);
	printf("threads_in_top_block = %d\n",threads_in_top_block);
#endif
	kernel_init_data_in_both_frontals <<< block_number, threads_per_block >>>(config, dev_frontal_matrices_p_x_p, dev_frontal_matrices_2x2);
	
	// 1) przygotuj dane + bottom_up
	bottom_up_merge_first_phase<<< block_number, threads_per_block >>>( config, dev_frontal_matrices_2x2, dev_stack );
	
	// 2) bottom_up in top, root, backward_subst in top
	kernel_bottom_up_merge_second_phase_and_root_and_backward_subst_first_phase<<< 1, threads_in_top_block >>>(config,dev_frontal_matrices_2x2, dev_solver_solution_vector,dev_stack);
	
	// 3) backward_substitution
	kernel_backward_subst_second_phase<<< block_number, threads_per_block >>>(config, dev_solver_solution_vector, dev_stack, dev_frontal_matrices_p_x_p, dev_complete_solution_vector);
}


void calculateDataToPlotForCompleteSolution(){
	HANDLE_ERROR( cudaMalloc((void**)&dev_plot_complete_values, sizeof(DATA_TYPE) * (config.points_to_plot) ) );
	host_plot_complete_values = (DATA_TYPE*) malloc(sizeof(DATA_TYPE) * (config.points_to_plot));
	
	kernel_calculate_values_to_plot_for_complete_solution<<< config.phisical_blocks_number, config.threads_per_block >>> (config, dev_complete_solution_vector, dev_plot_complete_values);

	HANDLE_ERROR( cudaMemcpy(host_plot_complete_values, dev_plot_complete_values, sizeof(DATA_TYPE)*(config.points_to_plot), cudaMemcpyDeviceToHost) );
	
	
}


void printSolverSolution(){
	print_solver_solution_vector(dev_solver_solution_vector,config);
}

void cleanMemory(){
	free(host_plot_solver_values);
	HANDLE_ERROR( cudaFree(dev_plot_solver_values) );

	free(host_plot_complete_values);
	HANDLE_ERROR( cudaFree(dev_plot_complete_values) );

	Stack *host_stack = Stack::copy_from_device(dev_stack);
	
	host_stack->clean();
	
	Stack::clean_copied_stack(host_stack);
	free(host_solver_solution_vector);
	HANDLE_ERROR( cudaFree(dev_solver_solution_vector) );
	HANDLE_ERROR( cudaFree(dev_frontal_matrices_2x2) );
	HANDLE_ERROR( cudaFree(dev_frontal_matrices_p_x_p) );

	HANDLE_ERROR( cudaFree(dev_complete_solution_vector) );
	free(host_complete_solution_vector);

	HANDLE_ERROR( cudaDeviceReset() );
}


__global__ void kernel_calculate_values_to_plot_for_complete_solution(SolverConfiguration config, DATA_TYPE *dev_complete_solution_vector, DATA_TYPE *dev_plot_complete_values){
	unsigned int logic_tid = GET_LOGIC_TID;
	DATA_TYPE h = (Q - P) / (DATA_TYPE) (config.points_to_plot - 1);
	DATA_TYPE x;	

	while (logic_tid < config.points_to_plot){
		x = P + logic_tid * h;
		dev_plot_complete_values[logic_tid] = 0.0;

		// licz na podstawie x ktory to jest przedzial (czy tez element skonczony), i tylko w tym policzy wartosci 
		DATA_TYPE x0 = x - P;			
		unsigned int internal_id = (unsigned int)(x0 / (config.internal_len));
		
		
		if (internal_id == N){
			internal_id--; // http://makeameme.org/meme/IF-YOU-HAVE-7nhnfd 
		}
		
		unsigned int offset = (P_ORDER + 1) * internal_id; // przesuniecie w wektorze z ca�ym rozwiazaniem
		DATA_TYPE tmpVal = 0.0;
		for (int i = 0; i < P_ORDER + 1; i++){
			unsigned int p_order = config.p_orders_tab[i];
			DATA_TYPE factor = dev_complete_solution_vector[offset + i];				
			DATA_TYPE chiVal = chi(p_order, internal_id, x, config);
			tmpVal += factor * chiVal;		
		}
		dev_plot_complete_values[logic_tid] += tmpVal;

		dev_plot_complete_values[logic_tid] += u_peak(1,0, x, config);		
		logic_tid += GET_LOGIC_TID_INC;
	}

	__syncthreads();
}

void printCompleteDataToPlot(){
	printf("Data to plot\n");
	DATA_TYPE h = (Q - P) / (DATA_TYPE) (config.points_to_plot - 1);
	DATA_TYPE x = P;

	printf("ID ----|----- x ----|---- y\n");
	for( unsigned int i=0; i < (config.points_to_plot); i++ ) {
		printf("  %4d | %10.10f | %10.10f \n", i, x, host_plot_complete_values[i]);
		x += h;
	}
	printf("##END Data to plot\n");

}

void writeCompleteSolutionToFile(){
	printf("Saving data to file: %s\n", config.file_name_to_write_output.c_str());
		DATA_TYPE h = (Q - P) / (DATA_TYPE) (config.points_to_plot - 1);
		DATA_TYPE x = P;

		std::ofstream file;
		file.open (config.file_name_to_write_output.c_str());
		file << "#x -----|---- y\n";
		for( unsigned int i=0; i < (config.points_to_plot); i++ ) {
			file << x << " " << host_plot_complete_values[i] << "\n";
			x += h;
		}		
		file.close();
}


