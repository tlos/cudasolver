﻿#ifndef CUDADEVICE_H_
#define CUDADEVICE_H_


/*Listowanie urządzeń dostępnych i ustawianianie domyślnego urządzenia*/

inline int get_cuda_devices_count();
bool is_any_cuda_device_available();
void list_available_cuda_devices();
bool set_device(int);


inline int get_cuda_devices_count(){
	int deviceCount;
	HANDLE_ERROR( 
		cudaGetDeviceCount(&deviceCount) 
		);
	return deviceCount;
}


bool is_any_cuda_device_available(){
	return get_cuda_devices_count() > 0; 
}


bool set_device(int device_id){
	int dev_count = get_cuda_devices_count();
	if( device_id <  dev_count){
		HANDLE_ERROR(
				cudaSetDevice(device_id)
			);
		printf("Set device: %d\n",device_id);
		return true;
	}else{
		printf("Wrong device_id = %d\nList available devices to get ids.\n",device_id);
		return false;
	}
}

inline int 
	___ConvertSMVer2Cores(int major, int minor)
{
	// Defines for GPU Architecture types (using the SM version to determine the # of cores per SM
	typedef struct
	{
		int SM; // 0xMm (hexidecimal notation), M = SM Major version, and m = SM minor version
		int Cores;
	} sSMtoCores;

	sSMtoCores nGpuArchCoresPerSM[] =
	{
		{ 0x10,  8 }, // Tesla Generation (SM 1.0) G80 class
		{ 0x11,  8 }, // Tesla Generation (SM 1.1) G8x class
		{ 0x12,  8 }, // Tesla Generation (SM 1.2) G9x class
		{ 0x13,  8 }, // Tesla Generation (SM 1.3) GT200 class
		{ 0x20, 32 }, // Fermi Generation (SM 2.0) GF100 class
		{ 0x21, 48 }, // Fermi Generation (SM 2.1) GF10x class
		{ 0x30, 192}, // Kepler Generation (SM 3.0) GK10x class
		{ 0x35, 192}, // Kepler Generation (SM 3.5) GK11x class
		{   -1, -1 }
	};

	int index = 0;

	while (nGpuArchCoresPerSM[index].SM != -1)
	{
		if (nGpuArchCoresPerSM[index].SM == ((major << 4) + minor))
		{
			return nGpuArchCoresPerSM[index].Cores;
		}

		index++;
	}

	// If we don't find the values, we default use the previous one to run properly
	//printf("MapSMtoCores for SM %d.%d is undefined.  Default to use %d Cores/SM\n", major, minor, nGpuArchCoresPerSM[7].Cores);
	return nGpuArchCoresPerSM[7].Cores;
}

unsigned int get_cuda_dev_cores(cudaDeviceProp deviceProp){
		return	 ___ConvertSMVer2Cores(deviceProp.major, deviceProp.minor) * deviceProp.multiProcessorCount;
}

void list_available_cuda_devices(){
	int device_count = get_cuda_devices_count();

	if( !is_any_cuda_device_available() ){
		printf("There is no Cuda Capable Device !\n");
		return;
	}

	for(int device = 0; device < device_count; device++){
		cudaSetDevice(device);
        cudaDeviceProp deviceProp;
		int driverVersion = 0, runtimeVersion = 0;
        cudaGetDeviceProperties(&deviceProp, device);	
		cudaDriverGetVersion(&driverVersion);
        cudaRuntimeGetVersion(&runtimeVersion);
		
        printf("+++Device %d: \"%s\"\n", device, deviceProp.name);
		printf("\tCUDA Driver Version / Runtime Version          %d.%d / %d.%d\n", driverVersion/1000, (driverVersion%100)/10, runtimeVersion/1000, (runtimeVersion%100)/10);
        printf("\tCUDA Capability Major/Minor version number:    %d.%d\n", deviceProp.major, deviceProp.minor);
		printf("\tCuda Cores:                                    %d\n", get_cuda_dev_cores(deviceProp));
#if CUDART_VERSION >= 5000
        printf("\tMemory Clock rate:                             %.0f Mhz\n", deviceProp.memoryClockRate * 1e-3f);
        printf("\tMemory Bus Width:                              %d-bit\n",   deviceProp.memoryBusWidth);
#endif
	}

}



#endif