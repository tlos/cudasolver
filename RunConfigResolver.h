#ifndef RUN_CONFIG_RESOLVER
#define RUN_CONFIG_RESOLVER


#include <iostream>
#include "SolverConfiguration.h"
#include "GpuConfiguration.h"
#include "Configuration.h"

using namespace std;


class RunConfigResolver{
public:
    unsigned int blocks_number_max;
    unsigned int threads_number_default_per_block;
    unsigned int blocks_number_in_bottom_block;
    unsigned int thread_number_in_bottom_block;
    unsigned int threads_number_in_top_block;
	unsigned int device_id;

    RunConfigResolver(unsigned int device_id){
		this->device_id = device_id;
		init();
    }
    void print(){
        cout << "!!!! RuningConfig !!!!" << endl;
        cout << "blocks_number_max= " << blocks_number_max << endl;
        cout << "threads_number_default_per_block= " << threads_number_default_per_block << endl;
        cout << "blocks_number_in_bottom_block= " << blocks_number_in_bottom_block << endl;
        cout << "thread_number_in_bottom_block= " << thread_number_in_bottom_block << endl;
        cout << "threads_number_in_top_block= " << threads_number_in_top_block << endl;
		cout << "get_intervals_number= " << get_intervals_number() << endl; 
		cout << "get_multi_processor_number= " << get_multi_processor_number() << endl; 
		cout << "get_cuda_cores_number= " << get_cuda_cores_number() << endl; 
        cout << "!!!! END - RuningConfig !!!!" << endl;
    }

private:
     cudaDeviceProp deviceProp;

     void init(){
		init_cuda_properties();
        resolve_threads_number_default_per_block();
        resolve_blocks_number_max();
        resolve_thread_number_in_bottom_block();
        resolve_blocks_number_in_bottom_block();
        resolve_thread_number_in_top_block();

    }
	 
	void init_cuda_properties(){
        cudaGetDeviceProperties(&deviceProp, this->device_id);
	}

    void resolve_threads_number_default_per_block(){
        this->threads_number_default_per_block = SHOULD_BE_THREADS;
    }
    void resolve_blocks_number_max(){
        unsigned int a = get_intervals_number();
        unsigned int b = get_cuda_cores_number()*2;
        unsigned int c = 32768;
        this->blocks_number_max = min(c, max(a,b));
		
    }

    void resolve_thread_number_in_bottom_block(){
        if( get_intervals_number() < 128 ){
            unsigned int max_threads_per_block = MAX_THREADS_PER_BLOCK;
            this->thread_number_in_bottom_block = min(max_threads_per_block, get_intervals_number());
        }else{
            this->thread_number_in_bottom_block = SHOULD_BE_THREADS;
        }
    }

    void resolve_blocks_number_in_bottom_block(){
        if( get_intervals_number() < 128 ){
            this->blocks_number_in_bottom_block = 1;
        }else{
            unsigned int a = get_intervals_number() / thread_number_in_bottom_block;
            unsigned int b = blocks_number_max;
            this->blocks_number_in_bottom_block = min(a, b);
        }
    }
    void resolve_thread_number_in_top_block(){
        unsigned int max_threads_per_block = MAX_THREADS_PER_BLOCK;
        this->threads_number_in_top_block = min( get_intervals_number(), max_threads_per_block);
    }

	
	unsigned int get_multi_processor_number(){
		return deviceProp.multiProcessorCount;
	}
	unsigned int get_cuda_cores_number(){
		return	 _ConvertSMVer2Cores(deviceProp.major, deviceProp.minor) * deviceProp.multiProcessorCount;
	}
	unsigned int get_intervals_number(){
		return N;
	}

	inline int _ConvertSMVer2Cores(int major, int minor)
	{
		// Defines for GPU Architecture types (using the SM version to determine the # of cores per SM
		typedef struct
		{
			int SM; // 0xMm (hexidecimal notation), M = SM Major version, and m = SM minor version
			int Cores;
		} sSMtoCores;

		sSMtoCores nGpuArchCoresPerSM[] =
		{
			{ 0x10,  8 }, // Tesla Generation (SM 1.0) G80 class
			{ 0x11,  8 }, // Tesla Generation (SM 1.1) G8x class
			{ 0x12,  8 }, // Tesla Generation (SM 1.2) G9x class
			{ 0x13,  8 }, // Tesla Generation (SM 1.3) GT200 class
			{ 0x20, 32 }, // Fermi Generation (SM 2.0) GF100 class
			{ 0x21, 48 }, // Fermi Generation (SM 2.1) GF10x class
			{ 0x30, 192}, // Kepler Generation (SM 3.0) GK10x class
			{ 0x35, 192}, // Kepler Generation (SM 3.5) GK11x class
			{   -1, -1 }
		};

		int index = 0;

		while (nGpuArchCoresPerSM[index].SM != -1)
		{
			if (nGpuArchCoresPerSM[index].SM == ((major << 4) + minor))
			{
				return nGpuArchCoresPerSM[index].Cores;
			}

			index++;
		}

		// If we don't find the values, we default use the previous one to run properly
		return nGpuArchCoresPerSM[7].Cores;
	}
};

#endif