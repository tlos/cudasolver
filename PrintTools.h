#ifndef _PRINT_TOOLS_H_
#define _PRINT_TOOLS_H_

#include "SolverConfiguration.h"
#include <cstdio>
#include <cuda_runtime_api.h>

void printFrontalMatrices(FrontalMatrix2 *dev_matrices, SolverConfiguration &config){
	printf("\n### print FrontalMatrices ###\n");
	int size = N;
	FrontalMatrix2 *h_m = (FrontalMatrix2*)malloc(sizeof(FrontalMatrix2)*size);
	HANDLE_ERROR( cudaMemcpy(h_m, dev_matrices, sizeof(FrontalMatrix2)*size, cudaMemcpyDeviceToHost) );

	for( int i=0; i < size; i++ ) {
		FrontalMatrix2 f = h_m[i];
		printf("i=%d \n[\n",i);
		printf("%5.2f  %5.2f   |  %5.2f |  %5d\n", f.A[0][0],f.A[0][1],  f.B[0], f.X_id[0]  );
		printf("%5.2f  %5.2f   |  %5.2f |  %5d\n", f.A[1][0],f.A[1][1],  f.B[1], f.X_id[1]  );

		printf("]\n");
	}
	free(h_m);
	printf("\n### print FrontalMatrices END ###\n");
}

void print_solver_solution_vector( DATA_TYPE *dev_solver_solution_vector , SolverConfiguration &config)
{
	printf("\n### print_solver_solution_vector ###\n");
	int size = N + 1;
	DATA_TYPE *h_v = (DATA_TYPE*)malloc(size*sizeof(DATA_TYPE));
	HANDLE_ERROR( cudaMemcpy(h_v, dev_solver_solution_vector, sizeof(DATA_TYPE)*size, cudaMemcpyDeviceToHost) );
	for( int i=0; i < size; i++ ) {
		printf("\t %d=\t%f\n", i, h_v[i]);
	}
	
	free(h_v);
	printf("\n### print_solver_solution_vector END ###\n");
}

void print_complete_solution_vector(DATA_TYPE *dev_complete_solution_vector, SolverConfiguration &config){
	printf("\n### print_complete_solution_vector ###\n");
	int size = (N) * (P_ORDER + 1);
	DATA_TYPE *h_v = (DATA_TYPE*)malloc(size * sizeof(DATA_TYPE));
	HANDLE_ERROR( cudaMemcpy(h_v, dev_complete_solution_vector, sizeof(DATA_TYPE)*size, cudaMemcpyDeviceToHost) );
	for( int i=0; i < size; i++ ) {
		
		if (i % (P_ORDER + 1) == 0){
			printf("\n");
		}
		printf("\t %d=\t%f\n", i, h_v[i]);
	}
	
	free(h_v);
	printf("\n### print_complete_solution_vector END ###\n");
}


#endif
