﻿#ifndef __GPU__CONFIG_H___
#define __GPU__CONFIG_H___


/**
* konfigracja wątków gpu jeśli użyta była opcja --no-auto to te wartości są brane pod uwagę:
*/

#define THREADS_PER_BLOCK				512
#define PHISICAL_BLOCKS_NUMBER			512
#define THREADS_IN_TOP_BLOCK			512


/**
* Poniższe wartości nie powinny być zmieniane
*/
#define SHOULD_BE_BLOCKS				128
#define SHOULD_BE_THREADS				128
#define MAX_THREADS_PER_BLOCK			768
#define DEFAULT_DEVICE_ID				0

#endif