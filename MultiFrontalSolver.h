#ifndef __MULTI__FRONTAL__SOLVER__H
#define __MULTI__FRONTAL__SOLVER__H



#include "DataProvider.h"
#include "Configuration.h"
#include "Utils.h"
#include "cuda_runtime.h"
#include "device_launch_parameters.h"
#include "ComputingPhrase.h"
#include "Stack.h"
#include "PartialGaussElimination.h"

__global__ void bottom_up_merge_first_phase(SolverConfiguration config, FrontalMatrix2 *dev_frontal_matrices, Stack *dev_stack);
__global__ void kernel_bottom_up_merge_second_phase_and_root_and_backward_subst_first_phase(SolverConfiguration config, FrontalMatrix2 *dev_frontal_matrices,DATA_TYPE *dev_solution_vector,  Stack *dev_stack);
__global__ void kernel_backward_subst_second_phase(SolverConfiguration config, DATA_TYPE *dev_solver_solution_vector,  Stack *dev_stack, DATA_TYPE *dev_frontal_matrices_p_x_p, DATA_TYPE *dev_complete_solution_vector);
__global__ void kernel_init_data_in_both_frontals(SolverConfiguration config, DATA_TYPE *dev_frontal_matrices_p_x_p, FrontalMatrix2 *dev_frontal_matrices_2x2);

/**
* LogicTid	 - tid wyliczony, mi�dzy blokami. Unikalny w ca�ym gridzie.
* InternalId - id przedzia�u na kt�rym dzia�amy, powi�zane z LogicTid, ale LogicTid�w mo�e by� mniej
*/

__device__ void bottom_up_merge(SolverConfiguration &config, FrontalMatrix2 *dev_frontal_matrices_2x2, Stack *dev_stack, COMPUTING_PHASE computing_phase){
	unsigned int logic_tid = GET_LOGIC_TID;
	
	unsigned int init_level = 1;
	unsigned int end_level = config.levels_in_bottom_block;
	
	if( computing_phase == IN_TOP_BLOCK ){
		init_level = config.levels_in_bottom_block;
		end_level = config.levels_in_grid;
		logic_tid *= config.nodes_in_bottom_block;
	}
	
	for(unsigned int level = init_level; level < end_level; ++level ){
		unsigned int internal_id_inc = GET_LOGIC_TID_INC;
		if( computing_phase == IN_TOP_BLOCK )		{
			internal_id_inc *= config.nodes_in_bottom_block;
		}
		for( int internal_id = logic_tid; internal_id < N; internal_id += internal_id_inc ) 
		{
			if( internal_id % two_powered_times( level ) != 0 ) continue;

			unsigned int first_source_id = internal_id;
			unsigned int second_source_id = internal_id + two_powered_times( level - 1 );
			FrontalMatrix2 m1 = dev_frontal_matrices_2x2[ first_source_id ];
			FrontalMatrix2 m2 = dev_frontal_matrices_2x2[ second_source_id ];

			FrontalMatrix3 merged;
			MERGE_MATRICES(m1, m2, merged);

			if( internal_id != 0 ){
					swap_us(merged.A[0][0], merged.A[1][1]);
					swap_us(merged.A[0][1], merged.A[1][0]);

					swap_us(merged.A[2][0], merged.A[2][1]);
					swap_us(merged.A[0][2], merged.A[1][2]);

					swap_us(merged.B[0], merged.B[1]);
					swap_us(merged.X_id[0], merged.X_id[1]);
			}
			// podzielenie pierwszego wiersza
			DATA_TYPE factor = merged.A[0][0];

			merged.A[0][0] = 1.0;//factor;
			merged.A[0][1] /= factor;
			merged.A[0][2] /= factor;
			merged.B[0] /= factor;

			factor =merged.A[1][0];
			merged.A[1][0] -= factor*merged.A[0][0];
			merged.A[1][1] -= factor*merged.A[0][1];
			merged.A[1][2] -= factor*merged.A[0][2];
			merged.B[1] -= factor*merged.B[0];

			factor = merged.A[2][0];
			merged.A[2][0] -= factor*merged.A[0][0];
			merged.A[2][1] -= factor*merged.A[0][1];
			merged.A[2][2] -= factor*merged.A[0][2];
			merged.B[2] -= factor*merged.B[0];

			// wrzucamy na stos wynik
			dev_stack->push(internal_id, merged);

			// przygotwanie do wypchni�cia do g�ry
			FrontalMatrix2 new_frontal;
			// kopiowanie dw�ch dolnych wierszy bez pierwszej kolumny
			new_frontal.A[0][0] = merged.A[1][1];
			new_frontal.A[0][1] = merged.A[1][2];
			new_frontal.A[1][0] = merged.A[2][1];
			new_frontal.A[1][1] = merged.A[2][2];

			new_frontal.B[0] = merged.B[1];
			new_frontal.B[1] = merged.B[2];
			new_frontal.X_id[0] = merged.X_id[1];
			new_frontal.X_id[1] = merged.X_id[2];

			dev_frontal_matrices_2x2[ internal_id ] = new_frontal;
		}

		__syncthreads();
	}

}


__device__ void root( SolverConfiguration &config, DATA_TYPE *dev_solver_solution_vector ,  Stack *dev_stack)
{
	unsigned int logic_tid = GET_LOGIC_TID;
	unsigned int internal_id = logic_tid;
	if( internal_id == 0 )
	{
		FrontalMatrix3 merged;
		dev_stack->pop(internal_id, merged);
		
		// podzielenie pierwszego wiersza
		DATA_TYPE factor = merged.A[0][0];
		merged.A[0][0] = 1.0;//factor;
		merged.A[0][1] /= factor;
		merged.A[0][2] /= factor;
		merged.B[0] /= factor;

		factor = merged.A[1][0];
		merged.A[1][0] -= factor*merged.A[0][0];
		merged.A[1][1] -= factor*merged.A[0][1];
		merged.A[1][2] -= factor*merged.A[0][2];
		merged.B[1] -= factor*merged.B[0];

		factor = merged.A[2][1]/merged.A[1][1];
		merged.A[2][1] -= factor*merged.A[1][1];
		merged.A[2][2] -= factor*merged.A[1][2];
		merged.B[2] -= factor*merged.B[1];

		// podzielenie 3-go wiersza 
		merged.B[2] /= merged.A[2][2];
		merged.A[2][2] = 1.0;

		dev_solver_solution_vector[ merged.X_id[2] ] = merged.B[2];
		dev_solver_solution_vector[ merged.X_id[1] ] = (merged.B[1] - merged.A[1][2]*  dev_solver_solution_vector[ merged.X_id[2] ])/ merged.A[1][1];
		dev_solver_solution_vector[ merged.X_id[0]] = merged.B[0] -merged.A[0][1] *  dev_solver_solution_vector[ merged.X_id[1] ];
	
	}

	__syncthreads();
}

__device__ void backward_substitution(SolverConfiguration &config, DATA_TYPE *dev_solver_solution_vector,  Stack *dev_stack, COMPUTING_PHASE computing_phase)
{
	unsigned int logic_tid = GET_LOGIC_TID;

	unsigned int init_level = config.nodes_in_bottom_block - 2;
	unsigned int end_level = 0;
	
	if( computing_phase == IN_TOP_BLOCK ){
		init_level = config.levels_in_grid - 2;
		end_level = config.levels_in_bottom_block - 2;
		logic_tid *= config.nodes_in_bottom_block;
	}
	for( unsigned int level = init_level; level > end_level; --level )
	{
		unsigned int internal_id_inc = GET_LOGIC_TID_INC;
		if( computing_phase == IN_TOP_BLOCK )		{
			internal_id_inc *= config.nodes_in_bottom_block;
		}
		for( int internal_id = logic_tid; internal_id < N; internal_id += internal_id_inc )
		{

			if( internal_id % two_powered_times( level )  != 0 ) continue; 

			FrontalMatrix3 m;
			dev_stack->pop(internal_id, m);
			dev_solver_solution_vector[ m.X_id[0] ] = m.B[0] - m.A[0][1]*dev_solver_solution_vector[ m.X_id[1] ] - m.A[0][2]*dev_solver_solution_vector[ m.X_id[2] ];

		}
		__syncthreads();
	}
}


__global__ void bottom_up_merge_first_phase(SolverConfiguration config, FrontalMatrix2 *dev_frontal_matrices_2x2, Stack *dev_stack)
{
	bottom_up_merge(config,dev_frontal_matrices_2x2,dev_stack, IN_BOTTOM_BLOCK);
}

__global__ void kernel_bottom_up_merge_second_phase_and_root_and_backward_subst_first_phase(SolverConfiguration config, FrontalMatrix2 *dev_frontal_matrices_2x2,DATA_TYPE *dev_solver_solution_vector,  Stack *dev_stack)
{
	bottom_up_merge(config,dev_frontal_matrices_2x2,dev_stack, IN_TOP_BLOCK);
	root(config, dev_solver_solution_vector, dev_stack);
	backward_substitution(config, dev_solver_solution_vector, dev_stack, IN_TOP_BLOCK);
}


__global__ void kernel_backward_subst_second_phase(SolverConfiguration config, DATA_TYPE *dev_solver_solution_vector,  Stack *dev_stack, DATA_TYPE *dev_frontal_matrices_p_x_p, DATA_TYPE *dev_complete_solution_vector)
{
	backward_substitution(config, dev_solver_solution_vector, dev_stack, IN_BOTTOM_BLOCK);
	__syncthreads();
	partial_gauss_part_two(config, dev_solver_solution_vector, dev_frontal_matrices_p_x_p, dev_complete_solution_vector);
}

#endif