#ifndef __DATA_INIT_KERNEL_H
#define __DATA_INIT_KERNEL_H



#include "DataProvider.h"
#include "Configuration.h"
#include "Utils.h"
#include "cuda_runtime.h"
#include "device_launch_parameters.h"
#include "ComputingPhrase.h"
#include "Stack.h"


__global__ void kernel_init_data_in_both_frontals(SolverConfiguration config, DATA_TYPE *dev_frontal_matrices_p_x_p, FrontalMatrix2 *dev_frontal_matrices_2x2){

	unsigned int logic_tid = GET_LOGIC_TID;
	unsigned int internal_id_inc = GET_LOGIC_TID_INC;
	// uzupe�nienie wszystkich macierzy PxP
	for( int internal_id = logic_tid; internal_id < N; internal_id += internal_id_inc ) {
		get_frontal_p_matrix_for_internal_id(internal_id, dev_frontal_matrices_p_x_p, config);
	}

	__syncthreads();

	// czesciowa eliminacja gaussa
	// petla po wszystkich macierzach PxP
	for( int internal_id = logic_tid; internal_id < N; internal_id += internal_id_inc ) {
		int id1, id2, id3, id4, id5;

		// petla po wierszach odpowiadajacym funkcjom wyzszego rzedu
		for (int k = 0; k < P_ORDER - 1; k++){
		
			// petla po wszystkich nastepnych wierszach
			for (int i = k + 1; i < P_ORDER + 1; i++){

				// petla po kolumnach (od przekatnej do konca)
				for (int j = k; j < P_ORDER + 1; j++){
					id1 = get_A_idx_from_P_frontal(i, j, internal_id);
					id2 = get_A_idx_from_P_frontal(i, j, internal_id);
					id3 = get_A_idx_from_P_frontal( k, j, internal_id);
					id4 = get_A_idx_from_P_frontal( i, k, internal_id);
					id5 = get_A_idx_from_P_frontal( k, k, internal_id);
					dev_frontal_matrices_p_x_p[id1] = dev_frontal_matrices_p_x_p [id2] - dev_frontal_matrices_p_x_p[id3] * (dev_frontal_matrices_p_x_p[id4] / dev_frontal_matrices_p_x_p [id5]);					
				}

				// element z kolumny wyrazow wolnych
				id1 = get_B_idx_from_P_frontal(i, internal_id);
				id2 = get_B_idx_from_P_frontal(k, internal_id);
				id3 = get_A_idx_from_P_frontal(i, k, internal_id);
				id4 = get_A_idx_from_P_frontal(k, k, internal_id);
				dev_frontal_matrices_p_x_p[id1] = dev_frontal_matrices_p_x_p[id1] - dev_frontal_matrices_p_x_p[id2] * (dev_frontal_matrices_p_x_p[id3] / dev_frontal_matrices_p_x_p[id4]);

				// pozostale elementy z 'lower triangular' s� "dokladnie" zerami
				dev_frontal_matrices_p_x_p[id3] = 0.0;				
			}
		}		
	}
	__syncthreads();

	int index;
	// skopiowanie danych do macierzy 2x2
	for( int internal_id = logic_tid; internal_id < N; internal_id += internal_id_inc ) {
		unsigned int p = P_ORDER;

		FrontalMatrix2 frontal_2;
		index = get_A_idx_from_P_frontal(p-1, p-1, internal_id);
		frontal_2.A[0][0] = dev_frontal_matrices_p_x_p[index];

		index = get_A_idx_from_P_frontal(p-1, p, internal_id);
		frontal_2.A[0][1] = dev_frontal_matrices_p_x_p[index];
		
		index = get_A_idx_from_P_frontal(p, p-1, internal_id);
		frontal_2.A[1][0] = dev_frontal_matrices_p_x_p[index];
		
		index = get_A_idx_from_P_frontal(p, p, internal_id);
		frontal_2.A[1][1] = dev_frontal_matrices_p_x_p[index];

		index = get_B_idx_from_P_frontal(p-1, internal_id);
		frontal_2.B[0] = dev_frontal_matrices_p_x_p[index];
		
		index = get_B_idx_from_P_frontal(p, internal_id);
		frontal_2.B[1] = dev_frontal_matrices_p_x_p[index];

		frontal_2.X_id[0] = internal_id;
		frontal_2.X_id[1] = internal_id + 1;

		dev_frontal_matrices_2x2 [internal_id] = frontal_2;
	}
}


#endif