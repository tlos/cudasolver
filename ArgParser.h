#ifndef __ARG__PARSE__HELP
#define __ARG__PARSE__HELP

#include "SolverConfiguration.h"
#include "string.h"
#include "CudaDevice.h"

int parse_args_and_update_config(int , char *[], SolverConfiguration &);
void print_help();
void print_wrong_args_message();


#define HELP_CMD "--help"
#define OUTFILE_CMD "-o"
#define NO_AUTO_CONFIG_CMD "--no-auto"
#define LIST_DEVS_CMD "--list"
#define SET_DEV_CMD "-d"
#define PRINT_CONFIG_CMD "-pc"
#define USAGE "--help - shows this info\n\
-o file_name -spec file where to put result\n\
-d device_id -sets device to computation (default 0)\n\
-pc - print configuration\n\
--no-auto -disables auto resolve threads and blocks configuration\n\
--list -lists available cuda devices\n"

void print_help(){
	printf("How to use:%s\n",USAGE);
}
void print_wrong_args_message(){
	printf("Wrong arguments detected!\nIf you wand to see help, put option %\n",HELP_CMD);
}

int parse_args_and_update_config(int argc, char *argv[], SolverConfiguration &config){
	int i = 0;
	while( i + 1 < argc ){
		i++;
		if( strcmp(argv[i],HELP_CMD) == 0 ){
			print_help();
			return -1;
		}else if(strcmp(argv[i], OUTFILE_CMD ) == 0 ){
			if( i < argc - 1 ){
				config.is_file_to_write_output = true;				
				config.file_name_to_write_output = argv[i+1];
				i++;
				continue;
			}else{
				print_wrong_args_message();
				return -1;
			}
		}else if(strcmp(argv[i], SET_DEV_CMD ) == 0 ){
			if( i < argc - 1 ){		
				config.device_id = atoi(argv[i+1]);
				if( config.device_id < 0 ) {
					printf("Wrong device_id !!!\n");
					return -1;
				}
				i++;
				continue;
			}else{
				print_wrong_args_message();
				return -1;
			}
		}else if(strcmp(argv[i], NO_AUTO_CONFIG_CMD) == 0 ){
			config.is_auto_config_run = false;
			continue;
		}else if(strcmp(argv[i], LIST_DEVS_CMD) == 0 ){
			list_available_cuda_devices();
			return 0;
		}else if(strcmp(argv[i], PRINT_CONFIG_CMD) == 0 ){
			config.print_config = true;
			continue;
		}else{
			print_wrong_args_message();
			return -1;
		}
	}
	return 1;
}

#endif