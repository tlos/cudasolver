#ifndef __TIME_MEASSURE_H__
#define __TIME_MEASSURE_H__

#include "cuda_runtime.h"
#include "device_launch_parameters.h"
#include "Utils.h"

float measure_runtime( void (*fun)() ){
	cudaEvent_t start, stop;
	HANDLE_ERROR( cudaEventCreate(&start) );
	HANDLE_ERROR( cudaEventCreate(&stop) );
	HANDLE_ERROR( cudaEventRecord(start,0) );
	fun();
	HANDLE_ERROR( cudaEventRecord(stop,0) );	
	HANDLE_ERROR( cudaEventSynchronize(stop) );
	float time;
	HANDLE_ERROR( cudaEventElapsedTime(&time, start,stop) );
	return time;
}  


#endif